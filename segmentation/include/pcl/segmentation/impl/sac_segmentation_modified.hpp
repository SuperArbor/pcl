/*
 * Software License Agreement (BSD License)
 *
 *  Point Cloud Library (PCL) - www.pointclouds.org
 *  Copyright (c) 2009, Willow Garage, Inc.
 *  Copyright (c) 2012-, Open Perception, Inc.
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the copyright holder(s) nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id$
 *
 */

#ifndef PCL_SEGMENTATION_IMPL_SAC_SEGMENTATION_MODIFIED_H_
#define PCL_SEGMENTATION_IMPL_SAC_SEGMENTATION_MODIFIED_H_

#include <pcl/segmentation/sac_segmentation_modified.h>

// Sample Consensus methods
#include <pcl/sample_consensus/lmeds.h>
#include <pcl/sample_consensus/mlesac.h>
#include <pcl/sample_consensus/msac.h>
#include <pcl/sample_consensus/prosac.h>
#include <pcl/sample_consensus/ransac_modified.h>
#include <pcl/sample_consensus/rmsac.h>
#include <pcl/sample_consensus/rransac.h>
#include <pcl/sample_consensus/sac_modified.h>

// Sample Consensus models
#include <pcl/sample_consensus/sac_model.h>
#include <pcl/sample_consensus/sac_result.h>
#include <pcl/sample_consensus/sac_model_circle.h>
#include <pcl/sample_consensus/sac_model_circle3d.h>
#include <pcl/sample_consensus/sac_model_cone.h>
#include <pcl/sample_consensus/sac_model_cylinder.h>
#include <pcl/sample_consensus/sac_model_line.h>
#include <pcl/sample_consensus/sac_model_normal_parallel_plane.h>
#include <pcl/sample_consensus/sac_model_normal_plane.h>
#include <pcl/sample_consensus/sac_model_normal_plane_modified.h>
#include <pcl/sample_consensus/sac_model_normal_sphere.h>
#include <pcl/sample_consensus/sac_model_parallel_line.h>
#include <pcl/sample_consensus/sac_model_parallel_plane.h>
#include <pcl/sample_consensus/sac_model_perpendicular_plane.h>
#include <pcl/sample_consensus/sac_model_plane_modified.h>
#include <pcl/sample_consensus/sac_model_sphere.h>
#include <pcl/sample_consensus/sac_model_stick.h>
#include <pcl/memory.h> // for static_pointer_cast
#include <pcl/common/time.h>

#include <iostream>
#include <fstream>

//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointT> void
pcl::SACSegmentationModified<PointT>::segment(PointIndices& inliers,
                                              ModelCoefficients& model_coefficients,
                                              SACResult& result)
{
  // Copy the header information
  inliers.header = model_coefficients.header = input_->header;

  if (!initCompute()) {
    inliers.indices.clear();
    model_coefficients.values.clear();
    result.SetTerminateReason("Failed to initialize computation");
    return;
  }

  // Initialize the Sample Consensus model and set its parameters
  if (!initSACModel(model_type_, result)) {
    PCL_ERROR("[pcl::%s::segment] Error initializing the SAC model!\n",
              getClassName().c_str());
    deinitCompute();
    inliers.indices.clear();
    model_coefficients.values.clear();
    result.SetTerminateReason("Failed to initialize sac model");
    return;
  }
  // Initialize the Sample Consensus method and set its parameters
  initSAC(method_type_, result);

  if (!sac_->computeModel(result, 0)) {
    PCL_ERROR("[pcl::%s::segment] Error segmenting the model! No solution found.\n",
              getClassName().c_str());
    deinitCompute();
    inliers.indices.clear();
    model_coefficients.values.clear();
    result.SetTerminateReason("Failed to compute model");
    return;
  }

  // Get the model inliers
  sac_->getInliers(inliers.indices);

  // Get the model coefficients
  Eigen::VectorXf coeff(model_->getModelSize());
  sac_->getModelCoefficients(coeff);

  // If the user needs optimized coefficients
  if (optimize_coefficients_) {
    Eigen::VectorXf coeff_refined(model_->getModelSize());
    model_->optimizeModelCoefficients(inliers.indices, coeff, coeff_refined);
    model_coefficients.values.resize(coeff_refined.size());
    memcpy(model_coefficients.values.data(), coeff_refined.data(), coeff_refined.size() * sizeof(float));
    // Refine inliers
    model_->selectWithinDistance(coeff_refined, threshold_distance_, inliers.indices);
  }
  else {
    model_coefficients.values.resize(coeff.size());
    memcpy(model_coefficients.values.data(), coeff.data(), coeff.size() * sizeof(float));
  }
  // compute model error and store it in result.
  sac_->computeModelError(result);

  deinitCompute();
}

//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointT> int
pcl::SACSegmentationModified<PointT>::segmentPlanes(
  int planes_to_find,
  std::vector<PointIndices>& indices_plane_inliers,
  std::vector<Eigen::Vector4f>& models_planes,
  SACSegmentationResult& segmentation_result,
  double least_points_for_segment_ratio,
  std::function<bool(const Eigen::Vector4f&, const std::vector<Eigen::Vector4f>&)> filter)
{
  // Start ticking
  StopWatch stopWatch;
  stopWatch.reset();

  // Initializing
  segmentation_result.SetSacModel(static_cast<SacModel>(model_type_));
  segmentation_result.SetSacMethod(method_type_);
  segmentation_result.SetUseClusters(false);
  segmentation_result.SetClusterNumber(0);
  segmentation_result.SetTotalPointNumber(input_->size());
  indices_plane_inliers.clear();
  models_planes.clear();
  int least_points_for_segment =
      static_cast<int>(least_points_for_segment_ratio * input_->size());

  ModelCoefficients::Ptr coefficients(new ModelCoefficients);
  PointIndices::Ptr inliers(new PointIndices);
  IndicesPtr indices_remained(new Indices());
  indices_remained->resize(input_->size());
  for (std::size_t i = 0; i < input_->size(); ++i)
    (*indices_remained)[i] = static_cast<index_t>(i);

  int planes_found = 0;
  bool find_all = planes_to_find < 0;
  for (int i = 0; find_all || planes_found < planes_to_find; ++i) {
    PCL_DEBUG("[pcl::%s::segmentPlanes] ************** iteration %d **************\n",
              getClassName().c_str(),
              i);

    if (indices_remained->size() < least_points_for_segment) {
      PCL_ERROR("[pcl::%s::segmentPlanes] Not enough remained points for segmentation. "
                "Quiting segmentation.\n",
                getClassName().c_str());

      segmentation_result.SetTerminateReason("Not enough points for segmentation in remained points.");
      break;
    }

    SACResult result;
    result.SetUseClusters(false);
    // this set the indices of the input cloud as well as the corresponding normals, so
    // the latter does not have to be handled
    pcl::SACSegmentationModified<PointT>::setIndices(indices_remained);
    // segmenting
    pcl::SACSegmentationModified<PointT>::segment(*inliers, *coefficients, result);

    if (result.GetTerminateReason() != "normal") {
      segmentation_result.SetTerminateReason("Failed to segment.");
      break;
    }

    if (inliers->indices.size() < least_points_for_segment) {
      PCL_DEBUG("[pcl::%s::segmentPlanes] The last segmented plane has inliers less "
                "than least points for segment. "
                "Consider segmentation done.\n",
                getClassName().c_str());

      segmentation_result.SetTerminateReason("Not enough points for segmentation in last inliers.");
      break;
    }

    // store the segmenting results
    Eigen::Vector4f model_plane(coefficients->values[0],
                                coefficients->values[1],
                                coefficients->values[2],
                                coefficients->values[3]);

    if (filter) {
      bool filtered = filter(model_plane, models_planes);
      if (!filtered) {
        models_planes.push_back(model_plane);
        indices_plane_inliers.push_back(*inliers);
        planes_found++;
      }
      segmentation_result.HandleNewSACResult(result, filtered);
    }
    else {
      models_planes.push_back(model_plane);
      indices_plane_inliers.push_back(*inliers);
      segmentation_result.HandleNewSACResult(result);
      planes_found++;
    }

    PCL_DEBUG("[pcl::%s::segmentPlanes] Segment a plane with %d inliers\n",
              getClassName().c_str(),
              inliers->indices.size());

    // update indices_remained by subtracting the inliers from it
    Indices difference;
    std::sort(indices_remained->begin(), indices_remained->end());
    std::sort(inliers->indices.begin(), inliers->indices.end());
    std::set_difference(indices_remained->begin(),
                        indices_remained->end(),
                        inliers->indices.begin(),
                        inliers->indices.end(),
                        std::back_inserter(difference));
    indices_remained = std::make_shared<Indices>(difference);
  }

  // update result
  segmentation_result.SetExecutionTime(stopWatch.getTime());
  segmentation_result.SummarizeResults();

  return planes_found;
}

//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointT> int
pcl::SACSegmentationModified<PointT>::segmentPlanes(
    int planes_to_find,
    const std::vector<Indices> clusters,
    std::vector<PointIndices>& indices_plane_inliers,
    std::vector<Eigen::Vector4f>& models_planes,
    SACSegmentationResult& segmentation_result,
    double least_points_for_segment_ratio,
    bool only_use_clustered,
    std::function<bool(const Eigen::Vector4f&, const std::vector<Eigen::Vector4f>&)> filter)
{
  // Start ticking
  StopWatch stopWatch;
  stopWatch.reset();

  // Initializing
  segmentation_result.SetSacModel(static_cast<SacModel>(model_type_));
  segmentation_result.SetSacMethod(method_type_);
  segmentation_result.SetUseClusters(true);
  segmentation_result.SetClusterNumber(clusters.size());
  segmentation_result.SetTotalPointNumber(input_->size());
  indices_plane_inliers.clear();
  models_planes.clear();
  int least_points_for_segment =
      static_cast<int>(least_points_for_segment_ratio * input_->size());

  // Get a copy of the clusters
  std::vector<Indices> clusters_copied = clusters;

  // Get unclustered indices
  ModelCoefficients::Ptr coefficients(new ModelCoefficients);
  PointIndices::Ptr inliers(new PointIndices);
  Indices indices_all, indices_all_clusters, indices_unclustered;
  for (auto& cluster : clusters_copied) {
    indices_all_clusters.insert(
        indices_all_clusters.end(), cluster.begin(), cluster.end());
  }
  indices_all.resize(input_->size());
  for (std::size_t i = 0; i < input_->size(); ++i)
    indices_all[i] = static_cast<index_t>(i);

  std::sort(indices_all_clusters.begin(), indices_all_clusters.end());
  std::set_difference(indices_all.begin(), indices_all.end(),
                      indices_all_clusters.begin(), indices_all_clusters.end(),
                      std::back_inserter(indices_unclustered));

  int planes_found = 0;
  bool find_all = planes_to_find < 0;
  bool using_uncluster = false;
  for (int i = 0; find_all || planes_found < planes_to_find; ++i) {
    PCL_DEBUG("[pcl::%s::segmentPlanes] \n************** iteration %d **************\n",
              getClassName().c_str(), i);

    size_t size_largest = 0;
    int idx_largest_cluster = 0;
    for (int i = 0; i < clusters_copied.size(); ++i) {
      size_t size = clusters_copied[i].size();
      if ( size > size_largest) {
        idx_largest_cluster = i;
        size_largest = size;
      }
    }
    SACResult result;
    result.SetUseClusters(true);
    // the cluster used for the current segmentation
    if (!using_uncluster) {
      if (clusters_copied[idx_largest_cluster].size() > least_points_for_segment) {
        PCL_DEBUG("[pcl::%s::segmentPlanes] segmenting with clustered points.\n",
                  getClassName().c_str());
        result.SetClusterIndex(idx_largest_cluster);
      }
      else if (!only_use_clustered) {
        PCL_DEBUG("[pcl::%s::segmentPlanes] not enough points in clustered points. switching to the unclustered.\n",
                  getClassName().c_str());
        using_uncluster = true;
        continue;
      }
      else {
        segmentation_result.SetTerminateReason(
            "Not enough points for segmentation in the current cluster; only_use_clustered specified.");
        break;
      }
    }
    else if (indices_unclustered.size() > least_points_for_segment) {
      PCL_DEBUG("[pcl::%s::segmentPlanes] segmenting with unclustered points.\n",
                getClassName().c_str());
      result.SetClusterIndex(-1);
    }
    else {
      PCL_DEBUG("[pcl::%s::segmentPlanes] Not enough remained points for segmentation. "
                "Quiting segmentation.\n",
                getClassName().c_str());
      segmentation_result.SetTerminateReason(
          "Not enough points for segmentation in unclustered points; using unclustered.");
      break;
    }

    // this set the indices of the input cloud as well as the corresponding normals, so
    // the latter does not have to be handled
    pcl::SACSegmentationModified<PointT>::setIndices(
        std::make_shared<Indices>((using_uncluster ? indices_unclustered : clusters_copied[idx_largest_cluster])));
    // segmenting
    segment(*inliers, *coefficients, result);

    if (result.GetTerminateReason() != "normal") {
      segmentation_result.SetTerminateReason("Failed to segment.");
      break;
    }

    if (inliers->indices.size() < least_points_for_segment) {
      if (!using_uncluster) {
        if (!only_use_clustered) {
          PCL_DEBUG("[pcl::%s::segmentPlanes] The last segmented plane for clustered "
                    "points has inliers less "
                    "than least points for segment. "
                    "Consider segmentation for unclustered.\n",
                    getClassName().c_str());

          using_uncluster = true;
          continue;
        }
        else {
          segmentation_result.SetTerminateReason(
              "Not enough points for segmentation in clustered points; only_use_clustered specified.");
          break;
        }
      }
      else {
        PCL_DEBUG("[pcl::%s::segmentPlanes] The last segmented plane for unclustered "
                  "points has inliers less "
                  "than least points for segment. "
                  "Consider segmentation done.\n",
                  getClassName().c_str());
        segmentation_result.SetTerminateReason(
            "Not enough points for segmentation in unclustered points; using unclustered.");
        break;
      }
    }

    // store the segmenting results
    Eigen::Vector4f model_plane(coefficients->values[0],
                                coefficients->values[1],
                                coefficients->values[2],
                                coefficients->values[3]);
    if (filter) {
      bool filtered = filter(model_plane, models_planes);
      if (!filtered) {
        models_planes.push_back(model_plane);
        indices_plane_inliers.push_back(*inliers);
        planes_found++;
      }
      segmentation_result.HandleNewSACResult(result, filtered);
    }
    else {
      models_planes.push_back(model_plane);
      indices_plane_inliers.push_back(*inliers);
      segmentation_result.HandleNewSACResult(result);
      planes_found++;
    }

    PCL_DEBUG("[pcl::%s::segmentPlanes] Segment a plane with %d inliers\n",
              getClassName().c_str(),
              inliers->indices.size());

    // update cluster_active by subtracting the calculated inliers from it
    Indices difference;
    if (!using_uncluster) {
      std::sort(clusters_copied[idx_largest_cluster].begin(),
                clusters_copied[idx_largest_cluster].end());
      std::sort(inliers->indices.begin(), inliers->indices.end());
      std::set_difference(clusters_copied[idx_largest_cluster].begin(),
                          clusters_copied[idx_largest_cluster].end(),
                          inliers->indices.begin(),
                          inliers->indices.end(),
                          std::back_inserter(difference));
      clusters_copied[idx_largest_cluster] = difference;
    }
    else {
      std::sort(indices_unclustered.begin(), indices_unclustered.end());
      std::sort(inliers->indices.begin(), inliers->indices.end());
      std::set_difference(indices_unclustered.begin(),
                          indices_unclustered.end(),
                          inliers->indices.begin(),
                          inliers->indices.end(),
                          std::back_inserter(difference));
      indices_unclustered = difference;
    }
  }
  // update result
  segmentation_result.SetExecutionTime(stopWatch.getTime());
  segmentation_result.SummarizeResults();

  return planes_found;
}

//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointT> bool
pcl::SACSegmentationModified<PointT>::initSACModel(const int model_type, SACResult& result)
{
  if (model_)
    model_.reset();

  // Build the model
  switch (model_type) {
  case SACMODEL_PLANE_MODIFIED: {
    PCL_DEBUG("[pcl::%s::initSACModel] Using a model of type: SACMODEL_PLANE\n",
              getClassName().c_str());
    model_.reset(new SampleConsensusModelPlaneModified<PointT>(input_, *indices_, random_));
    break;
  }
  case SACMODEL_NORMAL_PLANE_MODIFIED: 
  default: {
    PCL_ERROR("[pcl::%s::initSACModel] No valid model given!\n",
              getClassName().c_str());
    return (false);
  }
  }
  result.SetSacModel(static_cast<SacModel>(model_type));
  return (true);
}

//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointT> void
pcl::SACSegmentationModified<PointT>::initSAC(const int method_type, SACResult& result)
{
  if (sac_)
    sac_.reset();

  result.SetSacMethod(method_type);

      // Build the sample consensus method
  switch (method_type) {
  case SAC_RANSAC_MODIFIED:
  default: {
    PCL_DEBUG("[pcl::%s::initSAC] Using a method of type: SAC_RANSAC with a model threshold of %f\n",
              getClassName().c_str(),
              threshold_distance_);
    sac_.reset(new RandomSampleConsensusModified<PointT>(model_));
    break;
  }
  }
  // Set the Sample Consensus parameters if they are given/changed
  if (sac_->getDistanceThreshold() != threshold_distance_) {
    PCL_DEBUG("[pcl::%s::initSAC] Setting the desired distance threshold to %f\n",
              getClassName().c_str(),
              threshold_distance_);
    sac_->setDistanceThreshold(threshold_distance_);
  }

  if (sac_->getProbability() != probability_) {
    PCL_DEBUG("[pcl::%s::initSAC] Setting the desired probability to %f\n",
              getClassName().c_str(),
              probability_);
    sac_->setProbability(probability_);
  }
  if (max_iterations_ != -1 && sac_->getMaxIterations() != max_iterations_) {
    PCL_DEBUG("[pcl::%s::initSAC] Setting the maximum number of iterations to %d\n",
              getClassName().c_str(),
              max_iterations_);
    sac_->setMaxIterations(max_iterations_);
    result.SetMaxIterationNumber(max_iterations_);
  }
  if (samples_radius_ > 0.) {
    PCL_DEBUG("[pcl::%s::initSAC] Setting the maximum sample radius to %f\n",
              getClassName().c_str(),
              samples_radius_);
    // Set maximum distance for radius search during random sampling
    model_->setSamplesMaxDist(samples_radius_, samples_radius_search_);
  }
  if (sac_->getNumberOfThreads() != threads_) {
    PCL_DEBUG("[pcl::%s::initSAC] Setting the number of threads to %i\n",
              getClassName().c_str(),
              threads_);
    sac_->setNumberOfThreads(threads_);
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointT, typename PointNT> bool
pcl::SACSegmentationFromNormalsModified<PointT, PointNT>::initSACModel(const int model_type, SACResult& result)
{
  if (!input_ || !normals_) {
    PCL_ERROR("[pcl::%s::initSACModel] Input data (XYZ or normals) not given! Cannot continue.\n",
              getClassName().c_str());
    return (false);
  }
  // Check if input is synced with the normals
  if (input_->size() != normals_->size()) {
    PCL_ERROR("[pcl::%s::initSACModel] The number of points in the input point cloud "
              "differs than the number of points in the normals!\n",
              getClassName().c_str());
    return (false);
  }

  if (model_)
    model_.reset();

  result.SetSacModel(static_cast<SacModel>(model_type));

  // Build the model
  switch (model_type) {
  case SACMODEL_NORMAL_PLANE_MODIFIED: {
    PCL_DEBUG("[pcl::%s::initSACModel] Using a model of type: SACMODEL_NORMAL_PLANE_MODIFIED\n",
              getClassName().c_str());
    model_.reset(new SampleConsensusModelNormalPlaneModified<PointT, PointNT>(input_, *indices_, random_));
    typename SampleConsensusModelNormalPlaneModified<PointT, PointNT>::Ptr model_normals =
        static_pointer_cast<SampleConsensusModelNormalPlaneModified<PointT, PointNT>>(model_);
    // Set the input normals
    model_normals->setInputNormals(normals_);
    model_normals->setAllowObtuse(true);
    model_normals->setNormalAngleThreshold(threshold_normal_angle_);
    if (distance_weight_ != model_normals->getNormalDistanceWeight()) {
      PCL_DEBUG("[pcl::%s::initSACModel] Setting normal distance weight to %f\n",
                getClassName().c_str(),
                distance_weight_);
      model_normals->setNormalDistanceWeight(distance_weight_);
    }
    break;
  }
  // If nothing else, try SACSegmentationModified
  default: {
    return (pcl::SACSegmentationModified<PointT>::initSACModel(model_type, result));
  }
  }

  return (true);
}

#define PCL_INSTANTIATE_SACSegmentationModified(T)                                             \
  template class PCL_EXPORTS pcl::SACSegmentationModified<T>;
#define PCL_INSTANTIATE_SACSegmentationFromNormalsModified(T, NT)                              \
  template class PCL_EXPORTS pcl::SACSegmentationFromNormalsModified<T, NT>;

#endif // PCL_SEGMENTATION_IMPL_SAC_SEGMENTATION_MODIFIED_H_
