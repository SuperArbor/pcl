/*
 * Software License Agreement (BSD License)
 *
 *  Point Cloud Library (PCL) - www.pointclouds.org
 *  Copyright (c) 2010-2012, Willow Garage, Inc.
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the copyright holder(s) nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id$
 *
 */

#pragma once

#include <pcl/sample_consensus/sac_result.h>
#include <vector>
#include <string>
#include <chrono>
#include <pcl/common/time.h>
#include <boost/filesystem.hpp>

namespace pcl {
/**
 * @brief SACSegmentationResult is a class that contains the information of a *SegmentPlanes* task,
 * which means it has the result of multiple plane segmentation tasks.
*/
class SACSegmentationResult
{
public:
  SACSegmentationResult()
  {
    sac_model_ = SACMODEL_NONE;
    sac_method_ = SAC_NONE;
    use_clusters_ = false;
    time_execution_total_ = 0.;
    error_average_ = 0.;
    cluster_numbers_ = 0;
    n_points_total_ = 0;
    n_inliers_total_ = 0;
    terminate_reason_ = "normal";
  };

  bool
  WriteToFile(std::string path_file)
  {
    // clear file
    boost::filesystem::path path(path_file);
    if (path.has_parent_path())
      boost::filesystem::create_directories(path.parent_path());

    std::ofstream file;
    file.open(path_file, std::ios_base::out);
    file << "*********** Integrated segmentation information ***********" << std::endl << std::endl;
    file << "model type: " << sac_model_ << std::endl;
    file << "method type: " << sac_method_ << std::endl;
    file << "number of points in total: " << n_points_total_ << std::endl;
    file << "number of inliers in total: " << n_inliers_total_ << std::endl;
    file << "use clusters: " << (use_clusters_ ? "true" : "false") << std::endl;
    file << "number of clusters: " << cluster_numbers_ << std::endl;
    file << "execution time (ms): " << time_execution_total_ << std::endl;
    file << "average segmentation error: " << error_average_ << std::endl;
    file << "number of segmented planes: " << segmentation_results_.size() << std::endl;
    file << "number of valid segmented planes: " << segmentation_results_indices_valid_.size() << std::endl;
    file << "reason of termination: " << terminate_reason_ << std::endl;
    time_t now = time(nullptr);
    file << "report time: " << ctime(&now) << std::endl << std::endl;

    file << "*********** Detailed segmentation information ***********" << std::endl << std::endl;
    file.close();
    for (int i = 0; i < segmentation_results_.size(); i++) {
      SACResult result = segmentation_results_[i];
      bool valid = false;
      if (std::find(segmentation_results_indices_valid_.begin(),
                    segmentation_results_indices_valid_.end(),
                    i) != segmentation_results_indices_valid_.end()) {
        valid = true;
      }
      file.open(path_file, std::ios_base::out | std::ios_base::app);
      file << "Segmenting plane " << i << std::endl;
      file << "Valid: " << (valid ? "true" : "false") << std::endl;
      file.close();
      if (!result.WriteToFile(path_file, true)) {
        PCL_ERROR("Unable to write segment result to file %s.\n", path_file.c_str());
        return false;
      }
      file.open(path_file, std::ios_base::out | std::ios_base::app);
      file << std::endl;
      file.close();
    }
    
    return true;
  }

  void
  HandleNewSACResult(SACResult result, bool filtered = false)
  {
    if (!filtered) {
      segmentation_results_indices_valid_.push_back(segmentation_results_.size());
    }
    
    segmentation_results_.push_back(result);
  }

  void SummarizeResults()
  {
    double error_avg = 0.;
    size_t inliers_total = 0;
    for (SACResult result : segmentation_results_) {
      double error = result.GetError();
      size_t inliers = 0;
      if (result.GetValidInliersNumber() > 0) {
        inliers = result.GetValidInliersNumber();
      }
      else {
        inliers = result.GetInliersNumber();
      }
      error_avg += error * inliers;
      inliers_total += inliers;
    }
    error_avg /= inliers_total;
    error_average_ = error_avg;
    n_inliers_total_ = inliers_total;
  }

  void
  ClearSACResults()
  {
    segmentation_results_.clear();
    segmentation_results_indices_valid_.clear();
  }

  const std::vector<SACResult> &
  GetSACResults()
  {
    return segmentation_results_;
  }

  std::vector<SACResult>
  GetValidSACResults()
  {
    std::vector<SACResult> results;
    for (auto& idx : segmentation_results_indices_valid_) {
      results.push_back(segmentation_results_[idx]);
    }
    return results;
  }

  void    SetSacModel(SacModel model){ sac_model_ = model; }

  SacModel  GetSacModel() { return sac_model_; }

  void    SetSacMethod(int method){ sac_method_ = method; }

  int     GetSacMethod() { return sac_method_; }

  bool    GetUserClusters() { return use_clusters_; }

  void    SetUseClusters(bool use_clusters) { use_clusters_ = use_clusters; }

  void    SetClusterNumber(int n_clusters) { cluster_numbers_ = n_clusters; }

  int     GetClusterNumber() { return cluster_numbers_; }

  void    SetExecutionTime(double time){ time_execution_total_ = time; }

  double  GetExecutionTime() { return time_execution_total_; }

  double  GetAverageError() { return error_average_; }

  void    SetAverageError(double error) { error_average_ = error; }

  size_t  GetTotalPointNumber(){ return n_points_total_; }

  void    SetTotalPointNumber(size_t n_points){ n_points_total_ = n_points; }

  size_t  GetTotalInlierNumber(){ return n_inliers_total_; }

  void    SetTotalInlierNumber(size_t n_inliers){ n_inliers_total_ = n_inliers; }

  int     GetSegmentedPlanesNumber() { return segmentation_results_.size(); }

  void SetTerminateReason(std::string reason) { terminate_reason_ = reason;  }

  std::string GetTerminateReason() { return terminate_reason_; }

protected:
  std::vector<SACResult> segmentation_results_;
  std::vector<int> segmentation_results_indices_valid_;
  SacModel sac_model_;
  int sac_method_;
  bool use_clusters_;
  /**
   * @brief execution time of the segmentPlanes task.
   */
  double time_execution_total_;
  /**
   * @brief average error of all the segmentation tasks (for a single plane).
  */
  double error_average_;
  /**
   * @brief number of clusters used in segmenting.
  */
  int cluster_numbers_;
  /**
   * @brief number of points of the whole point cloud.
  */
  size_t n_points_total_;
  /**
   * @brief number of inliers of all the segmented planes in the segmentPlanes task.
  */
  size_t n_inliers_total_;
  /**
   * @brief the reason of termination
   */
  std::string terminate_reason_;
};
} // namespace pcl

