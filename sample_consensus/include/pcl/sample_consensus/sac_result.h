/*
 * Software License Agreement (BSD License)
 *
 *  Point Cloud Library (PCL) - www.pointclouds.org
 *  Copyright (c) 2010-2011, Willow Garage, Inc.
 *  Copyright (c) 2012-, Open Perception, Inc.
 *
 *  All rights reserved. 
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the copyright holder(s) nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id$
 *
 */

#pragma once

#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/method_types.h>

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <Eigen/Dense>
#include <vector>
#include <pcl/console/print.h>
#include <boost/filesystem.hpp>

namespace pcl {
/**
 * @brief SACRecord is a class that records the information of a single trial of plane segmentation task.
 * It is the element of SACResult.
*/
class SACRecord {
public:
  SACRecord()
  {
    n_iteration_ = 0;
    n_inliers_ = 0;
    k_ = 0.0;
    bad_sample_ = false;
    current_best_ = false;
  }

  SACRecord(int n_iteration,
            int n_inliers,
            double k,
            bool bad_sample,
            bool current_best,
            Eigen::VectorXf model_coefficients)
  {
    n_iteration_ = n_iteration;
    n_inliers_ = n_inliers;
    k_ = k;
    bad_sample_ = bad_sample;
    current_best_ = current_best;
    model_coefficients_ = model_coefficients;
  }

  int n_iteration_;
  size_t n_inliers_;
  /*k = log(1 - probability) / (log(1 - n_inliers_best / n_input)^sample_size)*/
  double k_;
  bool bad_sample_;
  bool current_best_;
  Eigen::VectorXf model_coefficients_;

};

/**
 * @brief SACResult is the result of a single plane segmentation task.
*/
class SACResult {
public:
  SACResult()
  {
    sac_model_ = SacModel::SACMODEL_NONE;
    sac_method_ = SAC_NONE;

    n_iteration_best_ = 0;
    time_execution_total_ = 0;
    n_iteration_max_ = 0;

    n_inliers_ = 0;
    n_inliers_valid_ = 0;
    record_all = false;
    idx_cluster_ = -1;
    error_ = 0;
    use_clusters_ = false;
    terminate_reason_ = "normal";
  }


  void Reset()
  {
    records_.clear();
    n_iteration_best_ = 0;
    time_execution_total_ = 0;
    n_iteration_max_ = 0;
    n_inliers_ = 0;
    n_inliers_valid_ = 0;
    idx_cluster_ = -1;
    error_ = 0;
    use_clusters_ = false;
    terminate_reason_ = "normal";
  }

  bool
  WriteToFile(std::string path_file, bool append=false)
  {
    boost::filesystem::path path(path_file);
    if (path.has_parent_path())
      boost::filesystem::create_directories(path.parent_path());

    std::ofstream file(path_file, (append ? std::ios_base::app : std::ios_base::out));
    // print format for model coefficients
    Eigen::IOFormat fmt(
        Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");
    if (file.is_open()) {
      // integrated report
      file << "model type: " << sac_model_ << std::endl;
      file << "method type: " << sac_method_ << std::endl;
      file << "use clusters: " << (use_clusters_ ? "true" : "false") << std::endl;
      file << "execution time(ms): " << time_execution_total_ << std::endl;
      file << "max iteration number: " << n_iteration_max_ << std::endl;
      file << "best iteration: " << n_iteration_best_ << std::endl;
      file << "inliers: " << n_inliers_ << std::endl;
      file << "inliers valid for error computation: " << n_inliers_valid_ << std::endl;
      file << "reason of termination: " << terminate_reason_ << std::endl;
      file << "number of records: " << records_.size() << std::endl;
      file << "index of cluster: " << idx_cluster_ << std::endl;
      file << "error of model: " << error_ << std::endl;
      file << "model coefficients: " << model_coefficients_.format(fmt) << std::endl << std::endl;

      // detailed report
      file << "iteration" << '\t' << "model" << '\t' << "inliers" << '\t'
           << "k" << std::endl;
      for (auto& record : records_) {
        file << record.n_iteration_ << '\t' << record.model_coefficients_.format(fmt) << '\t'
          << record.n_inliers_ << '\t' << record.k_ << std::endl;
      }

      file << std::endl;
      file.close();
    }
    else {
      PCL_ERROR("Unable to open file.");
      return false;
    }
    return true;
  }

  /**
   * @brief Handle a new record. Decide whether to store it according to conditions.
   * @param record 
  */
  void HandleNewRecord(SACRecord record)
  {
    if ((!record_all) && record.n_inliers_ <= n_inliers_) {
      return;
    }
    records_.push_back(record);
    if (record.n_inliers_ > n_inliers_) {
      n_inliers_ = record.n_inliers_;
      if (record.n_iteration_ >= 0) {
        // record.n_iteration_ == -1 when the record comes from out of the iterations, thus no need to record.
        n_iteration_best_ = record.n_iteration_;
      } 
      model_coefficients_ = record.model_coefficients_;
    }
    else if (record.model_coefficients_ != model_coefficients_) {
      model_coefficients_ = record.model_coefficients_;
    }
  }

  void    GetRecords(std::vector<SACRecord>& records){ records = records_; }

  void    SetBestIteration(int n_iteration){ n_iteration_best_ = n_iteration; }

  int     GetBestIteration() { return n_iteration_best_; }

  void    SetMaxIterationNumber(int n_iteration){ n_iteration_max_ = n_iteration; }

  int     GetMaxIterationNumber() { return n_iteration_max_; }

  void    SetExecutionTime(double time){ time_execution_total_ = time; }

  double  GetExecutionTime() { return time_execution_total_; }

  void    SetSacModel(SacModel model){ sac_model_ = model; }

  SacModel  GetSacModel() { return sac_model_; }

  void    SetSacMethod(int method){ sac_method_ = method; }

  int     GetSacMethod() { return sac_method_; }

  //void    SetInliersNumber(size_t n_inliers){ n_inliers_ = n_inliers; }

  size_t  GetInliersNumber() { return n_inliers_; }

  void    SetValidInliersNumber(size_t n_inliers){ n_inliers_valid_ = n_inliers; }

  size_t  GetValidInliersNumber() { return n_inliers_valid_; }

  void    SetClusterIndex(int idx) { idx_cluster_ = idx; }

  int     GetClusterIndex() { return idx_cluster_; }

  double  GetError() { return error_; }

  void    SetError(double error) { error_ = error; }

  bool    GetUserClusters() { return use_clusters_; }

  void    SetUseClusters(bool use_clusters) { use_clusters_ = use_clusters; }

  void    GetModelCoefficients(Eigen::VectorXf& model_coefficients) { model_coefficients = model_coefficients_; }

  //void    SetModelCoefficients(Eigen::VectorXf model_coefficients) { model_coefficients_ = model_coefficients; }

  void SetTerminateReason(std::string reason) { terminate_reason_ = reason;  }

  std::string GetTerminateReason() { return terminate_reason_; }

protected:
  std::vector<SACRecord> records_;
  int n_iteration_best_;
  int n_iteration_max_;
  /**
   * @brief execution time of the segmentation task of a single plane.
  */
  double time_execution_total_;
  SacModel sac_model_;
  int sac_method_;

  bool record_all;
  /**
   * @brief number of inliers of the segmented plane
  */
  size_t n_inliers_;
  /**
   * @brief number of inliers that are valid for error computation. it is obtained by calling sac_modified.computeModelError()
  */
  size_t n_inliers_valid_;
  int idx_cluster_;
  double error_;
  bool use_clusters_;
  Eigen::VectorXf model_coefficients_;
  std::string terminate_reason_;
};
} // namespace pcl
